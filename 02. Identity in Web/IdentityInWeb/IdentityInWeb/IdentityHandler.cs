﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IdentityInWeb
{
    public class IdentityHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Output.WriteLine($"IsAuth: {context.User.Identity.IsAuthenticated}");
            context.Response.Output.WriteLine("<br />");
            context.Response.Output.WriteLine($"User: {context.User.Identity.Name}");
        }
    }
}