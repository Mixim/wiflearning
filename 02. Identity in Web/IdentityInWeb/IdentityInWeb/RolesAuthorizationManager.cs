﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Xml;

namespace IdentityInWeb
{
    public class RolesAuthorizationManager : ClaimsAuthorizationManager
    {
        static Dictionary<ResourceAction, Func<ClaimsPrincipal, bool>> _policies = new Dictionary<ResourceAction, Func<ClaimsPrincipal, bool>>();

        /// <summary>
        /// Creates a new instance of the <see cref="RolesAuthorizationManager"/>.
        /// </summary>
        public RolesAuthorizationManager()
        {
        }

        /// <summary>
        /// Overloads  the base class method to load the custom policies from the config file
        /// </summary>
        /// <param name="nodelist">XmlNodeList containing the policy information read from the config file</param>
        public override void LoadCustomConfiguration(XmlNodeList nodelist)
        {
            PolicyReader policyReader = new PolicyReader();
            Expression<Func<ClaimsPrincipal, bool>> policyExpression;

            foreach (XmlNode node in nodelist)
            {
                if (node.NodeType != XmlNodeType.Comment)
                {
                    //
                    // Initialize the policy cache
                    //
                    using (XmlDictionaryReader rdr = XmlDictionaryReader.CreateDictionaryReader(new XmlTextReader(new StringReader(node.OuterXml))))
                    {
                        rdr.MoveToContent();

                        string resource = rdr.GetAttribute("resource");
                        string action = rdr.GetAttribute("action");

                        policyExpression = policyReader.ReadPolicy(rdr);

                        //
                        // Compile the policy expression into a function
                        //
                        Func<ClaimsPrincipal, bool> policy = policyExpression.Compile();

                        var newPolicyKey = new ResourceAction(resource, action);
                        //
                        // Insert the policy function into the policy cache
                        //
                        _policies[newPolicyKey] = policy;
                    }
                }
            }
        }

        /// <summary>
        /// Checks if the principal specified in the authorization context is authorized to perform action specified in the authorization context 
        /// on the specified resoure
        /// </summary>
        /// <param name="pec">Authorization context</param>
        /// <returns>true if authorized, false otherwise</returns>
        public override bool CheckAccess(AuthorizationContext pec)
        {
            //
            // Evaluate the policy against the claims of the 
            // principal to determine access
            //
            bool access = false;
            try
            {
                ResourceAction ra = new ResourceAction(pec.Resource.First<Claim>().Value, pec.Action.First<Claim>().Value);

                access = _policies[ra](pec.Principal);
            }
            catch (Exception)
            {
                access = false;
            }

            return access;
        }
    }
}