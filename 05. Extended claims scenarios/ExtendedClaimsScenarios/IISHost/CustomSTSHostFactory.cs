﻿using STSLibrary;
using System;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Security;

namespace IISHost
{
    public class CustomSTSHostFactory : ServiceHostFactoryBase
    {
        public override ServiceHostBase CreateServiceHost(string constructorString, Uri[] baseAddresses)
        {
            var config = new CustomSTSConfiguration();
            var returnValue = new WSTrustServiceHost(config, baseAddresses);
            return returnValue;
        }
    }
}