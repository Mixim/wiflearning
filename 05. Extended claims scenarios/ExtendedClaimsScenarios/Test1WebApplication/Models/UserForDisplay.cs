﻿

namespace Test1WebApplication.Models
{
    public class UserForDisplay
    {
        public bool IsAuthenticatedFromAuthService { get; set; }
        public string NameFromAuthService { get; set; }

        public string InfoFromWcfService { get; set; }

        public UserForDisplay(bool IsAuthenticatedFromAuthService, string NameFromAuthService, string InfoFromWcfService)
        {
            this.IsAuthenticatedFromAuthService = IsAuthenticatedFromAuthService;
            this.NameFromAuthService = NameFromAuthService;
            this.InfoFromWcfService = InfoFromWcfService;
        }

        public UserForDisplay(bool IsAuthenticatedFromAuthService, string NameFromAuthService)
            : this(IsAuthenticatedFromAuthService, NameFromAuthService, "")
        {
        }
    }
}