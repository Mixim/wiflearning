﻿using System.Configuration;
using System.IdentityModel.Tokens;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.WsFederation;
using Owin;

namespace Test1WebApplication
{
    public partial class Startup
    {
        #region Constants.

        /// <summary>
        /// Defines, that token sholud be save in <see cref="System.Security.Claims.ClaimsIdentity.BootstrapContext"/> for <see cref="System.Security.Claims.ClaimsPrincipal.Current"/>.
        /// </summary>
        protected const bool ShouldSaveSigninToken = true;

        #endregion

        private static string realm = ConfigurationManager.AppSettings["ida:Wtrealm"];
        private static string adfsMetadata = ConfigurationManager.AppSettings["ida:ADFSMetadata"];

        public void ConfigureAuth(IAppBuilder app)
        {
            app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);

            app.UseCookieAuthentication(new CookieAuthenticationOptions());

            var tokenValidationParameters = new TokenValidationParameters();
            tokenValidationParameters.SaveSigninToken = ShouldSaveSigninToken;

            app.UseWsFederationAuthentication(
                new WsFederationAuthenticationOptions
                {
                    Wtrealm = realm,
                    MetadataAddress = adfsMetadata,
                    TokenValidationParameters = tokenValidationParameters
                });
        }
    }
}