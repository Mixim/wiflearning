﻿using Microsoft.Owin;
using System.Linq;
using System.IdentityModel.Services;
using System.IdentityModel.Tokens;
using System.IO;
using System.Security.Claims;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using Test1WebApplication.Models;
using System.Security.Cryptography.X509Certificates;

namespace Test1WebApplication.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        #region Constants.

        protected const string RequiredEndpointNameOfWcfService = "ByCertificateEndpoint";

        protected const string UsedView = "Index";

        protected const string UsedThumbprint = "c82a67b6a058e19503c89d6d1f0cd7801e2760ee";

        #endregion

        public ActionResult Index()
        {
            IOwinContext owinContext;
            UserForDisplay currentUser;
            string nameFromAuthService;

            owinContext = HttpContext.GetOwinContext();
            nameFromAuthService = owinContext.Authentication.User.Claims.FirstOrDefault(curClaim => curClaim.Type == ClaimTypes.GivenName).Value;
            currentUser = new UserForDisplay(Request.IsAuthenticated, nameFromAuthService);
            return View(currentUser);
        }

        public ActionResult GoToWcfService()
        {
            IOwinContext owinContext;
            UserForDisplay currentUser;
            string nameFromAuthService;

            owinContext = HttpContext.GetOwinContext();
            nameFromAuthService = owinContext.Authentication.User.Claims.FirstOrDefault(curClaim => curClaim.Type == ClaimTypes.GivenName).Value;

            var bootstrapContext = ClaimsPrincipal.Current.Identities.First().BootstrapContext as BootstrapContext;

            var token = bootstrapContext.SecurityToken;
            if (token == null)
            {
                token = FederatedAuthentication.FederationConfiguration.IdentityConfiguration
                    .SecurityTokenHandlers.ReadToken(
                    new XmlTextReader(new StringReader(bootstrapContext.Token)));
            }

            var factory = new ChannelFactory<IdentityInSOAPServiceReference.IIdentityInSOAPService>(RequiredEndpointNameOfWcfService);


            factory.Credentials.ClientCertificate.SetCertificate(StoreLocation.LocalMachine, StoreName.My, X509FindType.FindByThumbprint, UsedThumbprint);
            factory.Credentials.ServiceCertificate.Authentication.CertificateValidationMode = System.ServiceModel.Security.X509CertificateValidationMode.None;

            IdentityInSOAPServiceReference.IIdentityInSOAPService wcfService = factory.CreateChannelWithActAsToken(token);
            string infoFromWcfService;
            infoFromWcfService = wcfService.GetInfo1();

            currentUser = new UserForDisplay(Request.IsAuthenticated, nameFromAuthService, infoFromWcfService);

            return View(UsedView, currentUser);
        }
    }
}