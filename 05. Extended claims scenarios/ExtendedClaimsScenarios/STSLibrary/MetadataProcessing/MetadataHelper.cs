﻿using System;
using System.IO;
using System.IdentityModel.Configuration;
using System.IdentityModel.Metadata;
using System.IdentityModel.Protocols.WSTrust;
using System.Xml;
using System.Text;
using System.Security.Claims;

namespace STSLibrary.MetadataProcessing
{
    /// <summary>
    /// Helper for working with metadata.
    /// </summary>
    public class MetadataHelper
    {
        #region Constants.

        protected const string WSTrust200502SupportedProtocol = "http://schemas.xmlsoap.org/ws/2005/02/trust";

        protected const string WSTrust200512SupportedProtocol = "http://docs.oasis-open.org/ws-sx/ws-trust/200512";

        protected const string WSFederation200706SupportedProtocol = "http://docs.oasis-open.org/wsfed/federation/200706";

        protected const string MetadataFilePath = "FederationMetadata.xml";

        #endregion

        #region Properties and fields.

        protected SecurityTokenServiceConfiguration ConfigurationForUse { get; set; }

        #endregion

        public MetadataHelper(SecurityTokenServiceConfiguration ConfigurationForUse)
        {
            this.ConfigurationForUse = ConfigurationForUse;
        }

        public string PrepareMetadata(MetadataPreparationParameters MetadataPreparationParams)
        {
            EntityDescriptor descriptor;
            string returnValue;

            descriptor = new EntityDescriptor(new EntityId(MetadataPreparationParams.EntityId));
            var stsDescriptor = new SecurityTokenServiceDescriptor();
            stsDescriptor.ServiceDisplayName = ConfigurationForUse.TokenIssuerName;

            stsDescriptor.ProtocolsSupported.Add(new Uri(WSTrust200502SupportedProtocol));
            stsDescriptor.ProtocolsSupported.Add(new Uri(WSTrust200512SupportedProtocol));
            stsDescriptor.ProtocolsSupported.Add(new Uri(WSFederation200706SupportedProtocol));

            EndpointReference stsTargetScopeForInsert;
            foreach (var currentTargetScope in MetadataPreparationParams.TargetScopes)
            {
                stsTargetScopeForInsert = currentTargetScope;
                stsDescriptor.TargetScopes.Add(stsTargetScopeForInsert);
                stsTargetScopeForInsert = null;
            }

            EndpointReference stsEndpointForInsert;
            foreach (var currentSTSEndpoint in MetadataPreparationParams.PassiveRequestorEndpoints)
            {
                stsEndpointForInsert = currentSTSEndpoint;
                stsDescriptor.PassiveRequestorEndpoints.Add(stsEndpointForInsert);
                stsEndpointForInsert = null;
            }

            foreach (var currentSTSEndpoint in MetadataPreparationParams.SecurityTokenServiceEndpoints)
            {
                stsEndpointForInsert = currentSTSEndpoint;
                stsDescriptor.SecurityTokenServiceEndpoints.Add(stsEndpointForInsert);
                stsEndpointForInsert = null;
            }

            stsDescriptor.ClaimTypesOffered.Add(new DisplayClaim(ClaimTypes.Name));
            stsDescriptor.ClaimTypesOffered.Add(new DisplayClaim(ClaimTypes.Role));
            stsDescriptor.ClaimTypesOffered.Add(new DisplayClaim(ClaimTypes.GroupSid));

            stsDescriptor.Keys.Add(new KeyDescriptor(ConfigurationForUse.SigningCredentials.SigningKeyIdentifier));

            descriptor.RoleDescriptors.Add(stsDescriptor);

            returnValue = SerializeMetadata(descriptor);

            return returnValue;
        }

        public string PrepareMetadataFile(MetadataPreparationParameters MetadataPreparationParams)
        {
            string metadata, returnValue;

            metadata = PrepareMetadata(MetadataPreparationParams);
            returnValue = Path.GetFullPath(MetadataFilePath);
            using (var metadataWriter = new StreamWriter(returnValue))
            {
                metadataWriter.Write(metadata);
            }

            return returnValue;
        }

        protected string SerializeMetadata(MetadataBase Metadata)
        {
            string returnValue;

            var serializer = new MetadataSerializer();
            var metadataDestination = new StringBuilder();
            using (XmlWriter writer = XmlWriter.Create(metadataDestination))
            {
                serializer.WriteMetadata(writer, Metadata);
            }

            returnValue = metadataDestination.ToString();

            return returnValue;
        }
    }
}