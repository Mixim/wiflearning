﻿using System.Collections.ObjectModel;
using System.IdentityModel.Protocols.WSTrust;

namespace STSLibrary.MetadataProcessing
{
    public class MetadataPreparationParameters
    {
        public string EntityId { get; set; }

        public Collection<EndpointReference> TargetScopes { get; set; }

        public Collection<EndpointReference> PassiveRequestorEndpoints { get; set; }

        public Collection<EndpointReference> SecurityTokenServiceEndpoints { get; set; }

        public MetadataPreparationParameters(string EntityId, Collection<EndpointReference> TargetScopes, Collection<EndpointReference> PassiveRequestorEndpoints, Collection<EndpointReference> SecurityTokenServiceEndpoints)
        {
            this.EntityId = EntityId;
            this.TargetScopes = TargetScopes;
            this.PassiveRequestorEndpoints = PassiveRequestorEndpoints;
            this.SecurityTokenServiceEndpoints = SecurityTokenServiceEndpoints;
        }
    }
}