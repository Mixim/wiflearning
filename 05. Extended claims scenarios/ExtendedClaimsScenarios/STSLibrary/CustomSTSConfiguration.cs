﻿using System.Collections.Generic;
using System.IdentityModel.Configuration;
using System.IdentityModel.Selectors;
using System.IdentityModel.Tokens;
using System.ServiceModel.Security;
using STSLibrary.Common;

namespace STSLibrary
{
    public class CustomSTSConfiguration : SecurityTokenServiceConfiguration
    {
        #region Constants.

        protected const string UsedIssuerName = "Local SSO";

        protected const string ThumbprintOfSigningCredentials = "C82A67B6A058E19503C89D6D1F0CD7801E2760EE";

        #endregion

        public CustomSTSConfiguration()
            : base(UsedIssuerName)
        {
            SigningCredentials = CertificateUtil.GetSigningCredentials(ThumbprintOfSigningCredentials);
            SecurityTokenService = typeof(CustomSTS);

            CertificateValidationMode = X509CertificateValidationMode.None;
            AudienceRestriction.AudienceMode = AudienceUriMode.Never;

            IssuerNameRegistry = new SimpleIssuerNameRegistry();

            var samlSecurityTokenHandler = new SamlSecurityTokenHandler();
            var saml2SecurityTokenHandler = new Saml2SecurityTokenHandler();

            samlSecurityTokenHandler.Configuration = SecurityTokenHandlers.Configuration;
            saml2SecurityTokenHandler.Configuration = SecurityTokenHandlers.Configuration;

            var handlersForUse = new List<SecurityTokenHandler>();
            handlersForUse.Add(samlSecurityTokenHandler);
            handlersForUse.Add(saml2SecurityTokenHandler);
            var actAsCollection = new SecurityTokenHandlerCollection(handlersForUse);

            SecurityTokenHandlerCollectionManager[SecurityTokenHandlerCollectionManager.Usage.ActAs] = actAsCollection;
        }
    }
}