﻿using System.IdentityModel.Tokens;
using System.Security.Cryptography.X509Certificates;

namespace STSLibrary.Common
{
    public static class CertificateUtil
    {
        public static SigningCredentials GetSigningCredentials(string Thumbprint)
        {
            X509Certificate2 usedCertificate;
            SigningCredentials returnValue;
            using (var store = new X509Store(StoreName.My, StoreLocation.LocalMachine))
            {
                store.Open(OpenFlags.ReadOnly);
                usedCertificate = store.Certificates.Find(X509FindType.FindByThumbprint, Thumbprint, false)[0];
            }

            returnValue = new X509SigningCredentials(usedCertificate);
            return returnValue;
        }
    }
}