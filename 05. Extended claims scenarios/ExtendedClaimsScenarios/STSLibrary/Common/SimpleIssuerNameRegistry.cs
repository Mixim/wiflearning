﻿using System;
using System.IdentityModel.Tokens;
using System.Security.Cryptography.X509Certificates;

namespace STSLibrary.Common
{
    /// <summary>
    /// Simple issuer name registry.
    /// </summary>
    public class SimpleIssuerNameRegistry : IssuerNameRegistry
    {
        public override string GetIssuerName(SecurityToken securityToken)
        {
            var x509Token = securityToken as X509SecurityToken;
            string returnValue;

            if (x509Token == null)
            {
                returnValue = string.Empty;
            }
            else
            {
                X509Certificate2 certificate = x509Token.Certificate;

                returnValue = certificate.Subject;
            }

            return returnValue;
        }
    }
}