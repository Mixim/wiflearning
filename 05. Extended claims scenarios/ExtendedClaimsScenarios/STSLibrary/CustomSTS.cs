﻿using System;
using System.Collections.Generic;
using System.IdentityModel;
using System.IdentityModel.Configuration;
using System.IdentityModel.Protocols.WSTrust;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;

namespace STSLibrary
{
    public class CustomSTS : SecurityTokenService
    {
        #region Constants.

        protected const string UsersRoleName = "Org Employees";

        protected const string StudentGroupGuid = "AAAAAAAA-BBBB-CCCC-DDDD-EEEEEEEEEEE1";
        protected const string TrainerGroupGuid = "AAAAAAAA-BBBB-CCCC-DDDD-EEEEEEEEEEE2";
        protected const string OtherGroupGuid = "AAAAAAAA-BBBB-CCCC-DDDD-EEEEEEEEEEE3";

        protected const string StudentLastname = "Golev";
        protected const string TrainerLastname = "Romanov";

        protected const string IdentityServerForTests = "https://localhost:44374/IdentityService.svc";
        protected const string IdentityServerForTask2 = "https://localhost:44371";
        protected const string IdentityServerForTask3 = "https://localhost:44328";
        protected const string IdentityServerForTask5 = "https://localhost:44364/";

        protected const string InvalidRequestForScopeExceptionFormat = "Invalid request for scope \"{0}\".";

        protected const string InvalidPrincipalNameExceptionFormat = "Principal name=\"{0}\" is invalid.";

        #endregion

        #region Properties and fields.

        protected readonly IDictionary<string, Guid> Groups;

        protected readonly IList<Uri> IdentityServersUrl;

        #endregion

        public CustomSTS(SecurityTokenServiceConfiguration securityTokenServiceConfiguration)
            : base(securityTokenServiceConfiguration)
        {
            Groups = new Dictionary<string, Guid>();
            IdentityServersUrl = new List<Uri>();
            var serverOfTests = new Uri(IdentityServerForTests);
            IdentityServersUrl.Add(serverOfTests);
            var serverOfIdentityInWeb = new Uri(IdentityServerForTask2);
            IdentityServersUrl.Add(serverOfIdentityInWeb);
            var serverOfIdentityInSOAP = new Uri(IdentityServerForTask3);
            IdentityServersUrl.Add(serverOfIdentityInSOAP);
            var serverOfIdentityInWeb2 = new Uri(IdentityServerForTask5);
            IdentityServersUrl.Add(serverOfIdentityInWeb2);
        }

        protected override ClaimsIdentity GetOutputClaimsIdentity(ClaimsPrincipal principal, RequestSecurityToken request, Scope scope)
        {
            IIdentity identityForProcessing;
            ClaimsIdentity returnValue;

            returnValue = new ClaimsIdentity();
            if (request.ActAs == null)
            {
                identityForProcessing = principal.Identity;
            }
            else
            {
                identityForProcessing = request.ActAs.GetIdentities().First();
            }
            string name;
            if(string.IsNullOrEmpty(identityForProcessing.Name))
            {
                if(identityForProcessing is ClaimsIdentity)
                {
                    name = (identityForProcessing as ClaimsIdentity).Claims.Single(curClaim => curClaim.Type == ClaimTypes.GivenName).Value;
                }
                else
                {
                    throw new ArgumentException(string.Format(InvalidPrincipalNameExceptionFormat, identityForProcessing.Name));
                }
            }
            else
            {
                name = identityForProcessing.Name;
            }
            returnValue.AddClaim(new Claim(ClaimTypes.Name, name));
            returnValue.AddClaim(new Claim(ClaimTypes.Role, UsersRoleName));
            if (name.Contains(StudentLastname))
            {
                returnValue.AddClaim(new Claim(ClaimTypes.GroupSid, StudentGroupGuid));
            }
            else
            {
                if(name.Contains(TrainerLastname))
                {
                    returnValue.AddClaim(new Claim(ClaimTypes.GroupSid, TrainerGroupGuid));
                }
                else
                {
                    returnValue.AddClaim(new Claim(ClaimTypes.GroupSid, OtherGroupGuid));
                }
            }

            return returnValue;
        }

        protected override Scope GetScope(ClaimsPrincipal principal, RequestSecurityToken request)
        {
            Scope returnValue;

            if (IdentityServersUrl.Any(curUrl => curUrl.IsBaseOf(request.AppliesTo.Uri)))
            {
                returnValue = new Scope(request.AppliesTo.Uri.ToString());
                returnValue.TokenEncryptionRequired = false;
                returnValue.SymmetricKeyEncryptionRequired = false;
                returnValue.ReplyToAddress = returnValue.AppliesToAddress;
            }
            else
            {
                throw new InvalidRequestException(string.Format(InvalidRequestForScopeExceptionFormat, request.AppliesTo.Uri));
            }
            return returnValue;
        }
    }
}