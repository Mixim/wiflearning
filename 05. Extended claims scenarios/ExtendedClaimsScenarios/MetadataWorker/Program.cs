﻿using System;
using System.Collections.ObjectModel;
using System.IdentityModel.Protocols.WSTrust;
using STSLibrary;
using STSLibrary.MetadataProcessing;

namespace MetadataWorker
{
    class Program
    {
        static void Main(string[] args)
        {
            const string entityId = "Local SSO";
            var stsConfig = new CustomSTSConfiguration();
            var metadataHelper = new MetadataHelper(stsConfig);
            string pathToPreparedMetadataFile;
            MetadataPreparationParameters metadataPreparationParams;
            var targetScopes = new Collection<EndpointReference>();
            var passiveRequestorEndpoints = new Collection<EndpointReference>();
            var securityTokenServiceEndpoints = new Collection<EndpointReference>();

            targetScopes.Add(new EndpointReference("https://localhost:44374/IdentityService.svc"));
            targetScopes.Add(new EndpointReference("https://localhost:44371"));
            targetScopes.Add(new EndpointReference("https://localhost:44328"));
            targetScopes.Add(new EndpointReference("https://localhost:44364/"));

            passiveRequestorEndpoints.Add(new EndpointReference("https://localhost:44381/fedauth"));

            securityTokenServiceEndpoints.Add(new EndpointReference("https://localhost:44381/Win"));
            securityTokenServiceEndpoints.Add(new EndpointReference("https://localhost:44381/UserName"));
            securityTokenServiceEndpoints.Add(new EndpointReference("https://localhost:44381/Cert"));

            metadataPreparationParams = new MetadataPreparationParameters(entityId, targetScopes, passiveRequestorEndpoints, securityTokenServiceEndpoints);

            pathToPreparedMetadataFile = metadataHelper.PrepareMetadataFile(metadataPreparationParams);
            Console.WriteLine("You can find prepared metadata file of Custom Security Token Service by the next path: \"{0}\".", pathToPreparedMetadataFile);
            Console.ReadKey();
        }
    }
}