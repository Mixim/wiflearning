﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;

namespace Test2WebApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.Use(async (context, next) =>
            {
                try
                {
                    await next();
                }
                catch (Exception ex)
                {
                    throw;
                }

            });
            ConfigureAuth(app);
        }
    }
}
