﻿using AngleSharp;
using System;
using System.Collections.Generic;
using System.IdentityModel.Configuration;
using System.IdentityModel.Protocols.WSTrust;
using System.IdentityModel.Selectors;
using System.IdentityModel.Services;
using System.IdentityModel.Tokens;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using System.Xml;
using NUnit.Framework;

namespace TestClient
{
    [TestFixture]
    public class STSTests
    {
        #region Constants.

        protected const string ThumbprintOfSigningCredentials = "C82A67B6A058E19503C89D6D1F0CD7801E2760EE";

        #endregion

        #region Environments of tests.

        private static IEnumerable<object[]> wsTrustStsBindings =>
            new List<object[]> {
                    new object[] {
                        "https://localhost:44381/STS.svc/ByWinUrl",
                        "https://localhost:44374/IdentityService.svc/someEndpoint",
                        ThumbprintOfSigningCredentials,
                        new WS2007HttpBinding()
                            {
                                Security = new WSHttpSecurity
                                {
                                    Mode = SecurityMode.TransportWithMessageCredential,
                                    Message = new NonDualMessageSecurityOverHttp
                                    {
                                        ClientCredentialType = MessageCredentialType.Windows
                                    }
                                }
                            },
                        null
                    },
                new object[] {
                        "https://localhost:44381/STS.svc/ByUserNameUrl",
                        "https://localhost:44374/IdentityService.svc/someEndpoint",
                        ThumbprintOfSigningCredentials,
                        new WS2007HttpBinding()
                            {
                                Security = new WSHttpSecurity
                                {
                                    Mode = SecurityMode.TransportWithMessageCredential,
                                    Message = new NonDualMessageSecurityOverHttp
                                    {
                                        ClientCredentialType = MessageCredentialType.UserName
                                    }
                                }
                            },
                        (Action<ClientCredentials>)((ClientCredentials cc) =>
                        {
                            cc.UserName.UserName = "User1";
                            cc.UserName.Password = "Password1";
                        })
                    },
                new object[] {
                        "https://localhost:44381/STS.svc/ByUserNameUrl",
                        "https://localhost:44374/IdentityService.svc/someEndpoint",
                        ThumbprintOfSigningCredentials,
                        new WS2007HttpBinding()
                            {
                                Security = new WSHttpSecurity
                                {
                                    Mode = SecurityMode.TransportWithMessageCredential,
                                    Message = new NonDualMessageSecurityOverHttp
                                    {
                                        ClientCredentialType = MessageCredentialType.UserName
                                    }
                                }
                            },
                        (Action<ClientCredentials>)((ClientCredentials cc) =>
                        {
                            cc.UserName.UserName = "User2";
                            cc.UserName.Password = "Password2";
                        })
                    },
            };

        private static IEnumerable<object[]> wsFederationStsBindings =>
            new List<object[]> {
                    new object[] {
                        "https://localhost:44381/FedAuth",
                        "https://localhost:44374/IdentityService.svc/someEndpoint",
                        ThumbprintOfSigningCredentials
                    }
            };

        #endregion

        public static void ReadTokenFromXml(XmlReader reader, string thumbprint)
        {
            var identityConfiguration = new IdentityConfiguration(false);
            identityConfiguration.AudienceRestriction.AudienceMode = AudienceUriMode.Never;
            var registry = identityConfiguration.IssuerNameRegistry as ConfigurationBasedIssuerNameRegistry;
            if (!registry.ConfiguredTrustedIssuers.ContainsKey(thumbprint))
            {
                registry.AddTrustedIssuer(thumbprint, "Local SSO");
            }

            var token = identityConfiguration.SecurityTokenHandlers.ReadToken(reader);
            var identities = identityConfiguration.SecurityTokenHandlers.ValidateToken(token);
            var userGroups = identities[0].Claims.Where(curClaim => curClaim.Type == ClaimTypes.GroupSid);
            
            Console.WriteLine("User name from token is \"{0}\".", identities[0].Name);
            if(userGroups.Any())
            {
                Console.WriteLine("This user is part of next groups:");
                foreach(var currentUserGroup in userGroups)
                {
                    Console.WriteLine("\"{0}\"", currentUserGroup.Value);
                }
            }
            else
            {
                Console.WriteLine("This user is not a part of any groups");
            }
        }

        [Test]
        [TestCaseSource(nameof(wsTrustStsBindings))]
        public void WSTrustTest(string stsAddress, string appliesTo, string signingKeyThumbprint, Binding binding, Action<ClientCredentials> credentialInitializer)
        {
            var channelFactory = new WSTrustChannelFactory();
            channelFactory.Endpoint.Address = new EndpointAddress(stsAddress);
            channelFactory.Endpoint.Binding = binding;

            credentialInitializer?.Invoke(channelFactory.Credentials);

            channelFactory.TrustVersion = TrustVersion.WSTrust13;
            var channel = channelFactory.CreateChannel();

            var result = channel.Issue(new RequestSecurityToken(RequestTypes.Issue)
            {
                AppliesTo = new EndpointReference(appliesTo)
            });

            var reader = new XmlNodeReader(((GenericXmlSecurityToken)result).TokenXml);
            channelFactory.Close();
            ReadTokenFromXml(reader, signingKeyThumbprint);
        }

        [Test]
        [TestCaseSource(nameof(wsFederationStsBindings))]
        public void WSFederationTest(string ipEndpointAddress, string realm, string signingKeyThumbprint)
        {
            var signInRequestMessag = new SignInRequestMessage(new Uri(ipEndpointAddress), realm);
            var signInRequestQueryString = signInRequestMessag.WriteQueryString();

            var ipClient = new HttpClient(new HttpClientHandler()
            {
                AllowAutoRedirect = true,
                UseDefaultCredentials = true
            });

            var signInResponseString = ipClient.GetStringAsync(signInRequestQueryString).Result;
            var context = BrowsingContext.New();
            var document = context.OpenAsync(rsp => { rsp.Content(signInResponseString); }).Result;
            var wresultString = document.QuerySelector("input[name='wresult']").GetAttribute("value");

            var signInResponseMessage = new SignInResponseMessage(
                new Uri(ipEndpointAddress), wresultString);

            var wsFederationSerializer = new WSFederationSerializer(new WSTrust13RequestSerializer(),
                new WSTrust13ResponseSerializer());
            var securityTokenResponse = wsFederationSerializer.CreateResponse(signInResponseMessage,
                new WSTrustSerializationContext());

            var xmlReader = new XmlNodeReader(securityTokenResponse.RequestedSecurityToken.SecurityTokenXml);
            ReadTokenFromXml(xmlReader, signingKeyThumbprint);
        }
    }
}