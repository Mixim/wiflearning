﻿using STSLibrary;
using System.IdentityModel;
using System.IdentityModel.Services;
using System.Security.Claims;
using System.Web;

namespace IISHost
{
    public class STSHandler : IHttpHandler
    {
        #region Constants.

        protected const int InvalidContextCode = 500;

        #endregion

        public bool IsReusable
        {
            get
            {
                return true;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            WSFederationMessage message = WSFederationMessage.CreateFromUri(context.Request.Url);
            if (message != null)
            {
                switch (message)
                {
                    case SignInRequestMessage signIn:
                    {
                        var config = new CustomSTSConfiguration();
                        SecurityTokenService sts = config.CreateSecurityTokenService();

                        var response = FederatedPassiveSecurityTokenServiceOperations
                            .ProcessSignInRequest(signIn, ClaimsPrincipal.Current, sts);

                        response.Write(context.Response.Output);
                        break;
                    }
                    case SignOutRequestMessage signOut:
                    {
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
            }
            else
            {
                context.Response.StatusCode = InvalidContextCode;
            }
        }
    }
}