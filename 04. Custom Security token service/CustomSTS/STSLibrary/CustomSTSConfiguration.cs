﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Configuration;
using System.IdentityModel.Tokens;
using System.Linq;
using STSLibrary.Common;

namespace STSLibrary
{
    public class CustomSTSConfiguration : SecurityTokenServiceConfiguration
    {
        #region Constants.

        protected const string UsedIssuerName = "Local SSO";

        protected const string ThumbprintOfSigningCredentials = "C82A67B6A058E19503C89D6D1F0CD7801E2760EE";

        #endregion

        public CustomSTSConfiguration()
            : base(UsedIssuerName)
        {
            SigningCredentials = CertificateUtil.GetSigningCredentials(ThumbprintOfSigningCredentials);
            SecurityTokenService = typeof(CustomSTS);

            List<SecurityTokenHandler> tokensHandlerForRemove = SecurityTokenHandlers
                .Where(th => th.TokenType == typeof(UserNameSecurityToken))
                .ToList();
            tokensHandlerForRemove.ForEach(th => SecurityTokenHandlers.Remove(th));

            SecurityTokenHandlers.Add(new CustomUserNameSecurityTokenHandler());
        }
    }
}