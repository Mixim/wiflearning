﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IdentityModel.Tokens;
using System.Security.Claims;

namespace STSLibrary
{
    public class CustomUserNameSecurityTokenHandler : UserNameSecurityTokenHandler
    {
        #region Properties and fields.

        public override bool CanValidateToken
        {
            get
            {
                return true;
            }
        }

        protected IDictionary<string, string> UserNamesAndPassword { get; set; }

        #endregion

        public CustomUserNameSecurityTokenHandler()
        {
            UserNamesAndPassword = new Dictionary<string, string>();
            UserNamesAndPassword.Add("User1", "Password1");
            UserNamesAndPassword.Add("User2", "Password2");
        }

        public override ReadOnlyCollection<ClaimsIdentity> ValidateToken(SecurityToken token)
        {
            string actualPassword;
            var userNameST = token as UserNameSecurityToken;
            ReadOnlyCollection<ClaimsIdentity> returnValue;

            returnValue = null;
            if(UserNamesAndPassword.TryGetValue(userNameST.UserName, out actualPassword))
            {
                if (actualPassword == userNameST.Password)
                {
                    var userNameClaim = new Claim(ClaimTypes.Name, userNameST.UserName);
                    var claimList = new List<Claim>();
                    claimList.Add(userNameClaim);
                    var claimIdentityList = new List<ClaimsIdentity>();
                    claimIdentityList.Add(new ClaimsIdentity(claimList));
                    returnValue = new ReadOnlyCollection<ClaimsIdentity>(claimIdentityList);
                }
            }
            if (returnValue == null)
            {
                returnValue = base.ValidateToken(token);
            }
            return returnValue;
        }
    }
}