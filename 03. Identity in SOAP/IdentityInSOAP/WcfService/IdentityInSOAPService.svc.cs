﻿using System;
using System.Diagnostics;
using System.Security.Claims;
using System.Security.Permissions;
using System.Security.Principal;

namespace WcfService
{
    public class IdentityInSOAPService : IIdentityInSOAPService
    {
        #region Constants.

        protected const string OutputFormat = "Method name = \"{0}\";\nIs authenticated = \"{1}\";\nUsername = \"{2}\".";

        #endregion

        public IdentityInSOAPService()
        {
            AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);
        }

        [PrincipalPermission(SecurityAction.Demand, Role = "Org Employees")]
        public string GetInfo1()
        {
            StackTrace currentStackTrace;
            StackFrame currentStackFrame;
            ClaimsPrincipal currentPrincipal;
            string returnValue;

            currentStackTrace = new StackTrace();
            currentStackFrame = currentStackTrace.GetFrame(0);
            currentPrincipal = ClaimsPrincipal.Current;
            returnValue = string.Format(IdentityInSOAPService.OutputFormat, currentStackFrame.GetMethod().Name, currentPrincipal?.Identity.IsAuthenticated, currentPrincipal?.Identity.Name);
            return returnValue;
        }
    }
}