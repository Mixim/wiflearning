﻿using NUnit.Framework;
using IdentityInSOAPClient.WcfService;
using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.IdentityModel.Protocols.WSTrust;
using System.IdentityModel.Tokens;
using System.Xml;
using System.IdentityModel.Configuration;
using System.Security.Cryptography.X509Certificates;

namespace IdentityInSOAPClient
{
    [TestFixture]
    public class Client
    {
        #region Constants.

        protected const string WSTrustWinEndpointConfigName = "For Win IP";

        protected const string WSTrustUserNameEndpointConfigName = "For UserName IP";

        protected const string ServiceEndpointUri = "https://localhost:44328/IdentityInSOAPService.svc/someEndpoint2";

        protected const string ServiceEndpointConfigName = "For SP";

        protected const string MySSOThumbprint = "c82a67b6a058e19503c89d6d1f0cd7801e2760ee";

        protected const string MySSOName = "‎localhost";

        #endregion

        #region Case sources.

        protected static IEnumerable<object[]> GetAndSendTokenCases = new List<object[]>
        {
                    new object[]
                    {
                        Client.WSTrustWinEndpointConfigName,
                        "",
                        ""
                    },
                    new object[]
                    {
                        Client.WSTrustUserNameEndpointConfigName,
                        "User1",
                        "Password1"
                    }
        };

        #endregion

        [SetUp]
        public void InitTests()
        {
            ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => { return true; };
        }

        [Test]
        public void SimpleWCFAndWIF()
        {
            IdentityInSOAPServiceClient clientOfService = new IdentityInSOAPServiceClient();
            Console.WriteLine(clientOfService.GetInfo1());
        }

        [Test]
        [TestCaseSource(nameof(GetAndSendTokenCases))]
        public void GetAndSendToken(string endpointConfigName, string userName, string password)
        {
            var identityProviderChannelFactory = new WSTrustChannelFactory(endpointConfigName);
            identityProviderChannelFactory.TrustVersion = TrustVersion.WSTrust13;
            identityProviderChannelFactory.Credentials.UserName.UserName = userName;
            identityProviderChannelFactory.Credentials.UserName.Password = password;

            IWSTrustChannelContract identityProviderChannel = identityProviderChannelFactory.CreateChannel();

            var identityProviderResponce = identityProviderChannel.Issue(new RequestSecurityToken(RequestTypes.Issue)
            {
                AppliesTo = new EndpointReference(Client.ServiceEndpointUri)
            });

            var reader = new XmlNodeReader(((GenericXmlSecurityToken)identityProviderResponce).TokenXml);
            var identityConfiguration = new IdentityConfiguration();
            identityConfiguration.AudienceRestriction.AllowedAudienceUris.Add(new Uri(Client.ServiceEndpointUri));
            identityConfiguration.ServiceTokenResolver =
                new X509CertificateStoreTokenResolver(StoreName.My, StoreLocation.CurrentUser);
            if (!(identityConfiguration.IssuerNameRegistry as ConfigurationBasedIssuerNameRegistry).ConfiguredTrustedIssuers.ContainsKey(Client.MySSOThumbprint))
            {
                (identityConfiguration.IssuerNameRegistry as ConfigurationBasedIssuerNameRegistry)
                    .AddTrustedIssuer(Client.MySSOThumbprint, Client.MySSOName);
            }

            var token = identityConfiguration.SecurityTokenHandlers.ReadToken(reader);
            var identities = identityConfiguration.SecurityTokenHandlers.ValidateToken(token);

            foreach (var currentIdentity in identities)
            {
                foreach (var currentClaim in currentIdentity.Claims)
                {
                    Console.WriteLine(currentClaim);
                }
            }

            var serviceProviderChannelFactory = new ChannelFactory<IIdentityInSOAPService>(Client.ServiceEndpointConfigName);
            var serviceProviderChannel = serviceProviderChannelFactory.CreateChannel();

            Console.WriteLine(serviceProviderChannel.GetInfo1());
        }
    }
}