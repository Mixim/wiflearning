﻿using NUnit.Framework;
using System;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.IdentityModel.Tokens;
using System.IO;
using System.Xml;

namespace SecurityTokenAPITests
{
    [TestFixture]
    public class SamlAndJwtTokenTest
    {
        #region Constants.

        protected const string IssuerName = "EPAM SSO Test";

        protected const string Name = "Maksim Golev";

        protected const string NameIdentifier = "Maksim_Golev@epam.com";

        protected const string UpsaIdClaimType = "UpsaId";
        protected const ulong UpsaIdClaimValue = 4060741400040904622;

        protected const string WorkstationIdClaimType = "WorkstationId";
        protected const string WorkstationIdClaimValue = "EPRUIZHW0336";

        protected const string CertificateThumbprint = "369EC5A2CBD6C6924F7E403E11D743CCC1185D31";

        #endregion

        #region Readonly.

        protected readonly DateTime DateOfBirth = new DateTime(1991, 8, 20);

        #endregion

        #region Properties.

        protected ClaimsIdentity Identity { get; set; }

        #endregion

        [SetUp]
        public void Init()
        {
            Identity = new ClaimsIdentity();
            Identity.AddClaim(new Claim(ClaimTypes.Name, Name));
            Identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, NameIdentifier));
            Identity.AddClaim(new Claim(ClaimTypes.DateOfBirth, DateOfBirth.ToShortDateString()));
            Identity.AddClaim(new Claim(UpsaIdClaimType, UpsaIdClaimValue.ToString()));
            Identity.AddClaim(new Claim(WorkstationIdClaimType, WorkstationIdClaimValue));
        }

        [Test]
        [TestCase(typeof(Saml2SecurityTokenHandler))]
        [TestCase(typeof(JwtSecurityTokenHandler))]
        public void CreateToken(Type securityTokenType)
        {
            var securityTokenHandler = Activator.CreateInstance(securityTokenType) as SecurityTokenHandler;
            var securityTokenDescriptor = new SecurityTokenDescriptor()
            {
                TokenIssuerName = IssuerName,
                Subject = Identity
            };
            var token = securityTokenHandler.CreateToken(securityTokenDescriptor);
            SerializeAndPrintToken(securityTokenHandler, token);
        }

        [Test]
        public void CreateSignedToken()
        {
            var securityTokenHandler = new Saml2SecurityTokenHandler();

            var tripleDES = TripleDES.Create();
            tripleDES.GenerateKey();

            var encryptingKey = tripleDES.Key;

            var securityKey = new InMemorySymmetricSecurityKey(encryptingKey);
            var securityKeyIdentifier = new SecurityKeyIdentifier(
                        new EncryptedKeyIdentifierClause(encryptingKey,
                        SecurityAlgorithms.TripleDesEncryption)
                        );

            var encryptingCredentials = new EncryptingCredentials(securityKey, securityKeyIdentifier, SecurityAlgorithms.TripleDesEncryption);

            X509Certificate2 certificate;

            var store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly);

            var certificates = store.Certificates
                .Find(X509FindType.FindByThumbprint,
                CertificateThumbprint, false);

            certificate = certificates[0];

            var signingCredentials = new X509SigningCredentials(certificate);

            var token = securityTokenHandler.CreateToken(new SecurityTokenDescriptor
            {
                TokenIssuerName = IssuerName,
                Subject = Identity,
                SigningCredentials = signingCredentials,
                EncryptingCredentials = encryptingCredentials
            });

            SerializeAndPrintToken(securityTokenHandler, token);
        }

        private void SerializeAndPrintToken(SecurityTokenHandler handler, SecurityToken token)
        {
            if(token is Saml2SecurityToken)
            {
                var stringWriter = new StringWriter();
                var xmlWriter = XmlWriter.Create(stringWriter, new XmlWriterSettings() { Indent = true });

                handler.WriteToken(xmlWriter, token);
                xmlWriter.Close();

                Console.WriteLine(stringWriter);
            }
            else
            {
                if(token is JwtSecurityToken)
                {
                    var serializedToken = handler.WriteToken(token);
                    Console.WriteLine(string.Format("{0}\n\n", serializedToken));

                    var serializedTokenItems = serializedToken
                        .Split('.')
                        .Select(curItem => Base64UrlEncoder.Decode(curItem));

                    foreach(var currentItem in serializedTokenItems)
                    {
                        Console.WriteLine(currentItem);
                    }
                }
            }
        }
    }
}