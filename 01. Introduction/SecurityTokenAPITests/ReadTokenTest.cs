﻿using NUnit.Framework;
using Microsoft.IdentityModel.Extensions;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Xml;

namespace SecurityTokenAPITests
{
    [TestFixture]
    public class ReadTokenTest
    {
        private void PrintIdentity(ClaimsIdentity identity)
        {
            foreach (var claim in identity.Claims)
            {
                Console.WriteLine($"{claim.Type} : {claim.Value}");
            }
        }

        static ReadTokenTest()
        {
            System.Diagnostics.Debug.WriteLine(Properties.Resources.pathToSamlToken);
        }

        [OneTimeSetUp]
        public void Init()
        {
            Environment.CurrentDirectory = Path.GetDirectoryName(this.GetType().Assembly.Location);
        }

        [Test]
        public void ReadSamlToken_Error()
        {
            SecurityToken token;
            var tokenHandler = new SamlSecurityTokenHandler();
            tokenHandler.Configuration = new SecurityTokenHandlerConfiguration();

            using (var fileReader = new FileStream(Properties.Resources.pathToSamlToken, FileMode.Open))
            {
                var xmlReader = XmlReader.Create(fileReader);
                token = tokenHandler.ReadToken(xmlReader) as SamlSecurityToken;
            }

            var identities = tokenHandler.ValidateToken(token);

            var identity = identities.First();

            PrintIdentity(identity);
        }

        [Test]
        public void ReadJwtToken_Error()
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken token;

            using (var fileReader = new StreamReader(Properties.Resources.pathToJwtToken))
            {
                token = tokenHandler.ReadToken(fileReader.ReadToEnd());
            }
            var identities = tokenHandler.ValidateToken(token);

            var identity = identities.First();

            PrintIdentity(identity);
        }




        public static IEnumerable<TestCaseData> TokensAndHandlers
        {
            get
            {
                yield return new TestCaseData(
                        Properties.Resources.pathToSamlToken,
                        //new Microsoft.IdentityModel.Tokens.Saml2SecurityTokenHandler()
                        new Microsoft.IdentityModel.Tokens.SamlSecurityTokenHandler()
                    )
                    .SetName(Properties.Resources.pathToSamlToken);
                yield return new TestCaseData(
                    Properties.Resources.pathToJwtToken,
                        new JwtSecurityTokenHandler()
                        )
                        .SetName(Properties.Resources.pathToJwtToken);
            }
        }

        [Test]
        [TestCaseSource(nameof(TokensAndHandlers))]
        public void ReadToken2(string pathToToken, ISecurityTokenValidator tokenHandler)
        {
            string tokenString;
            SecurityToken token;

            using (var fileReader = new StreamReader(pathToToken))
            {
                tokenString = fileReader.ReadToEnd();
            }

            var principal = tokenHandler.ValidateToken(tokenString,
                new TokenValidationParameters
                {
                    RequireSignedTokens = false,
                    ValidateLifetime = false,
                    ValidateAudience = false,
                    ValidateIssuer = false
                }, out token);

            var identity = principal.Identities.First();

            PrintIdentity(identity);
        }


        private static IEnumerable<object[]> Tokens =>
            new List<object[]> {
                    new object[] { Properties.Resources.pathToJwtToken },
                    new object[] { Properties.Resources.pathToSamlToken }
            };


        [Test]
        [TestCaseSource(nameof(Tokens))]
        public void ReadAnyToken(string pathToToken)
        {
            string tokenString;
            var tokenHandlerCollection = new SecurityTokenHandlerCollection();
            tokenHandlerCollection.AddOrReplace(new JwtSecurityTokenHandler());
            tokenHandlerCollection.AddOrReplace(new
                Microsoft.IdentityModel.Tokens.SamlSecurityTokenHandler());


            SecurityToken token;

            using (var fileReader = new StreamReader(pathToToken))
            {
                tokenString = fileReader.ReadToEnd();
            }

            var principal = tokenHandlerCollection.ValidateToken(tokenString,
                new TokenValidationParameters
                {
                    RequireSignedTokens = false,
                    ValidateLifetime = false,
                    ValidateAudience = false,
                    ValidateIssuer = false
                }, out token);

            var identity = principal.Identities.First();

            PrintIdentity(identity);
        }
    }
}